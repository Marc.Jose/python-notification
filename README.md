# Python-Notification
[python-notification](https://gitlab.com/marcjose/python-notification) provides a python wrapper for the 
[notify-send](http://www.galago-project.org/specs/notification/0.9/index.html)
command on linux.
> - [Installation](#installation)
>     - [Using Package Installer (pip)](#using-package-installer-pip)
>     - [From Source](#from-source)
> - [Supported arguments](#supported-arguments)
>     - [Urgency Level](#urgency-level)
>     - [Expire time](#expire-time)
>     - [Application Name](#application-name)
>     - [Icon](#icon)
>     - [Category](#category)
>     - [Summary](#summary)
>     - [Body](#body)
> - [Example](#example)
> - [Running Tests](#running-tests)

## Installation
### Using Package Installer (pip)
The recommended way to install this library is by using `pip`:
```shell script
pip install git+https://gitlab.com/marcjose/python-notification.git
```
### From Source
You can also build and install the library from sources:
```shell script
wget "https://gitlab.com/marcjose/python-notification/-/archive/v1.0.0/python-notification-v1.0.0.zip"
unzip python-notification-v1.0.0.zip
cd python-notification-v1.0.0
python3 setup.py install
```
<br/><br/><br/><br/>
## Supported arguments
### Urgency Level
Currently there are 3 supported urgency levels: Low, Normal and Critical.
You can set them via:
```python
from notification import Notification, Urgency

notification = Notification()
notification.urgency = Urgency.LOW
```
The default value is `Urgency.NORMAL`.  
*For more information about their meaning visit http://www.galago-project.org/specs/notification/0.9/x320.html.*
### Expire time
The expiration time defines how long a notification should be visible in milliseconds and can be set via:
```python
from notification import Notification

notification = Notification()
notification.expire_time = 10000
```
The default value is `3000`.
### Application Name
The application name is shown at the very top of a notification and can be set via:
```python
from notification import Notification

notification = Notification()
notification.app_name = 'My SuperApp'
```
The default value is `Notification`.
### Icon
The icon is shown next to the notification message.
It can either be a so called stock icon which can be any image from `/usr/share/icons/*/32x32/**/*.png` or
an absolute path to an image file.
```python
import os
from notification import Notification

# List all available stock icons
print(Notification.stock_icons)

# Set icon
notification = Notification()
notification.icon = Notification.stock_icons[0] # or
Notification.icon = os.path.join('img', 'icon.png')
```
### Category
Currently there are 20 predefined category type:   
device, device.added, device.error, device.removed, 
email, email.arrived, email.bounced, 
im, im.error, im.received, 
network, network.connected, network.disconnected, network.error, 
presence, presence.offline, presence.online, 
transfer, transfer.complete, transfer.error  
Custom category names are also supported.
You can set them via:
```python
from notification import Notification, Category

notification = Notification()
notification.category = Category.DEVICE_ADDED # or
notification.category = 'custom.category'
```
The default value is `Category.IM`.  
*For more information about their meaning visit http://www.galago-project.org/specs/notification/0.9/x211.html.*
### Summary
The summary is a mandatory field and contains the header of the notification. 
It  can be set via:
```python
from notification import Notification

notification = Notification()
notification.summary = 'Notification Title'
```
### Body
The body is a mandatory field and contains the actual message. 
It can contain some HTML and special tags and can be set via:
```python
from notification import Notification

notification = Notification()
notification.body = '<b>Hello World!</b>'
```
*For more information visit http://www.galago-project.org/specs/notification/0.9/x161.html.*
<br/><br/><br/><br/>
## Example
```python
# Import libraries
from notification import Notification, Category, Urgency

# Create new notification with default values
notification = Notification()

# Set urgency level (default: Urgency.NORMAL)
notification.urgency = Urgency.NORMAL

# Set time in milliseconds that the notification will be displayed (default: 3000)
notification.expire_time = 3000

# Set the notification title that is unique for this application (default: 'Notification')
notification.app_name = 'Notification'

# Set the notification icon (default: None)
notification.icon = None

# Set the notification category (default: Category.IM)
notification.category = Category.IM

# Set the notification title
notification.summary = 'Super Important Notification'

# Set the notification body/message
notification.body = 'Example Notification<br/>This is just an example notification'

# Send the notification
notification.send()

# Or as a single call:
Notification(
    urgency=Urgency.NORMAL,
    expire_time=3000,
    app_name='Notification',
    icon=None,
    category=Category.IM,
    summary='Super Important Notification',
    body='Example Notification<br/>This is just an example notification'
).send()
```
<br/><br/><br/><br/>
## Running Tests
To run the tests with coverage execute `python3 setup.py test`.
