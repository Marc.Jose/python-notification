# -*- coding: UTF-8 -*-
""" Enum for all predefined category types """

from enum import Enum, unique


@unique
class Category(Enum):
    """ Category types based on http://www.galago-project.org/specs/notification/0.9/x211.html """

    # A generic device-related notification that doesn't fit into any other category
    DEVICE = 'device'
    # A device, such as a USB device, was added to the system
    DEVICE_ADDED = 'device.added'
    # A device had some kind of error
    DEVICE_ERROR = 'device.error'
    # A device, such as a USB device, was removed from the system
    DEVICE_REMOVED = 'device.removed'
    # A generic e-mail-related notification that doesn't fit into any other category
    EMAIL = 'email'
    # A new e-mail notification
    EMAIL_ARRIVED = 'email.arrived'
    # A notification stating that an e-mail has bounced
    EMAIL_BOUNCED = 'email.bounced'
    # A generic instant message-related notification that doesn't fit into any other category
    IM = 'im'
    # An instant message error notification
    IM_ERROR = 'im.error'
    # A received instant message notification
    IM_RECEIVED = 'im.received'
    # A generic network notification that doesn't fit into any other category
    NETWORK = 'network'
    # A network connection notification, such as successful sign-on to a network service
    # This should not be confused with device.added for new network devices
    NETWORK_CONNECTED = 'network.connected'
    # A network disconnected notification
    # This should not be confused with device.removed for disconnected network devices
    NETWORK_DISCONNECTED = 'network.disconnected'
    # A network-related or connection-related error
    NETWORK_ERROR = 'network.error'
    # A generic presence change notification that doesn't fit into any other category,
    # such as going away or idle
    PRESENCE = 'presence'
    # An offline presence change notification
    PRESENCE_OFFLINE = 'presence.offline'
    # An online presence change notification
    PRESENCE_ONLINE = 'presence.online'
    # A generic file transfer or download notification that doesn't fit into any other category
    TRANSFER = 'transfer'
    # A file transfer or download complete notification
    TRANSFER_COMPLETE = 'transfer.complete'
    # A file transfer or download error
    TRANSFER_ERROR = 'transfer.error'

    @staticmethod
    def from_str(category: str) -> 'Category':
        """ Convert string to enum based on the name. """
        category = category.lower().strip()
        for entry in Category:
            if entry.value == category:
                return entry
        raise ValueError(f'"{category}" is not a valid category!')
