# -*- coding: UTF-8 -*-
""" Enum for all available urgency types """

from enum import IntEnum, unique


@unique
class Urgency(IntEnum):
    """ Urgency types based on http://www.galago-project.org/specs/notification/0.9/x320.html """

    LOW = 0
    NORMAL = 1
    CRITICAL = 2

    @staticmethod
    def from_str(urgency: str) -> 'Urgency':
        """ Convert string to enum based on the name """
        urgency = urgency.upper().strip()
        for entry in Urgency:
            if entry.name == urgency:
                return entry
        raise ValueError(f'"{urgency}" is not a valid urgency level!')
