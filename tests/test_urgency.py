#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
""" Pytest Suite """

import unittest
from notification import Notification, Urgency


class UrgencyTestCase(unittest.TestCase):
    """
    Test if urgency levels can be correctly set and parsed.
    """

    def test_urgency_levels(self) -> None:
        """ Test setting notification urgency levels to all possible values. """
        notification = Notification()
        for urgency_level in notification.urgency_levels:
            notification.urgency = urgency_level
            self.assertEqual(notification.urgency, urgency_level)

    def test_urgency_low(self) -> None:
        """ Test if 'low' is correctly translated to the enum. """
        low_urgency = Urgency.from_str('LoW')
        self.assertEqual(low_urgency, Urgency.LOW)

    def test_urgency_normal(self) -> None:
        """ Test if 'normal' is correctly translated to the enum. """
        normal_urgency = Urgency.from_str('normal')
        self.assertEqual(normal_urgency, Urgency.NORMAL)

    def test_urgency_critical(self) -> None:
        """ Test if 'critical' is correctly translated to the enum. """
        critical_urgency = Urgency.from_str('CRITICAL')
        self.assertEqual(critical_urgency, Urgency.CRITICAL)

    def test_urgency_invalid(self) -> None:
        """ Test if nonsense string raises an error when converting to enum. """
        with self.assertRaises(ValueError):
            Urgency.from_str('nonsense')
